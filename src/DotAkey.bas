
'
' project : DotA Keyboard
'
' status :
' x - press on/ press off button..
' x - send key 2 pc
' x hero name,skill, and key code label
' x menu for choose hero (ask user what's hero for play)
' - combo key (back to home)
' - (easy) change key
' - setup key procedure -> save to mcu/eeprom
' - save new hero

'******************************************************************************
'  Project : PS/2 Keyboard for DotA Gamer
'  By : BenNueng
'  Date : 4/01/2007 (new) : 27/07/2006(Old)
'  MCU : ATMEGA32 8Mhz
'  Compiler : BasComAVR
'
'******************************************************************************
' becareful: can't push 3 buttons at a same time like tri-angle.
' it's will make square effect...
'******************************************************************************



'================================================
'
'
' data for load at start ,save in eeprom
'
$eeprom
Data 0                                                      ' free

' 25 byte reserve for general code (start at 0 and don't use 0)
General_cfg:
Data 0 , 0 , 0 , 0 , 0 , 0
Data 0 , 0 , 0 , 0 , 0 , 0
Data 0 , 0 , 0 , 0 , 0 , 0
Data 0 , 0 , 0 , 0 , 0 , 0

Custom_keytable_code:
'  keyboard 6x4  from  ETT
'  F1    1     SK1      SK2      SK3      SK4
'  A     H     O        4        5        [
'  Shft  UP    U        1        2        ]
'  LEFT  DOWN  RIGHT    Combo    Ctrl     ALT
'
Data 56 , 28 , 3 , 15 , 5 , 20
Data 1 , 8 , 15 , 93 , 94 , 71
Data 45 , 78 , 21 , 90 , 91 , 99
Data 79 , 80 , 81 , 254 , 46 , 48

'======== default button type ==================================================
' 1 = normal key     = press on , release off
' 2 = numeric key    = press on , release off (automatic turn numlock key on and off after use)
' 3 = special key    = press once on, press again off, press other key until special key on. 
' 4 = combo key      = press on , release off, but can set macro key + use with mouse ps/2
' 25 = skill 1 key      = looklike normal key but for change in system use by each hero
' 26 = skill 2 key      = looklike normal key but for change in system use by each hero
' 27 = skill 3 key      = looklike normal key but for change in system use by each hero
' 28 = skill 4 key      = looklike normal key but for change in system use by each hero
' 29 = skill 5 key(OPT) = looklike normal key but for change in system use by each hero
' 30 = skill 6 key(OPT) = looklike normal key but for change in system use by each hero
'
Custom_keytable_type:
Data 1 , 1 , 25 , 26 , 27 , 28
Data 1 , 1 , 1 , 2 , 2 , 3
Data 1 , 1 , 4 , 2 , 2 , 3
Data 1 , 1 , 1 , 2 , 2 , 3


Custom_hero:
' 38 byte number 1,sk1,sk2,sk3,sk4,sk5,sk6,name 30+1
Data 255 , 1 , 2 , 3 , 4 , 5 , 6 , "Noname Hero                   "

'===============================================================================


'$regfile = "m8535.dat"
$regfile = "M32def.dat"
$crystal = 8000000
'$crystal = 11059200 ' error 0.0% @19200
$baud = 19200

'$hwstack = 300                                               ' default use 32 for the hardware stack
'$swstack = 150                                              ' default use 10 for the SW stack
'$framesize = 80                                             ' default use 40 for the frame space
$lib "mcsbyteint.lbx"                                       ' use optional lib since we use only bytes

'-------------- set program ---------------------------------------------------
'const _speed_send2pc = 50
'Const _timer_set = 30720                                    ' 30720; 10 ms @ 36.86mhz
Const _program_name = "DotA Key Killer"
Const _firmware_version = "1.02"

Const _adr_custom_config = 1
Const _adr_custom_key_code = _adr_custom_config + 24
Const _adr_custom_key_type = _adr_custom_key_code + 24
Const _adr_custom_hero = _adr_custom_key_type + 24          ' @73


Const _specialkey_debounce = 35                             ' 35*8.16 = 280ms @clock 8mhz
Const _numlock_wait = 150                                   ' 50*8.16 = 400ms @clock 8mhz
Const _debounce = 25                                        ' 25 ms
Const _repeater_wait = 10

Const _max_buffer = 6
Const _all_key = 24
Const _dec_all_key = _all_key - 1                           ' for loop
Const _dec_max_buffer = _max_buffer - 1

'Const _2dec_max_buffer = _max_buffer - 2
'Const _max_special_key = 3

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Row0 Alias Pina.0
Row1 Alias Pina.1                                           ' Keyboard Rows
Row2 Alias Pina.2
Row3 Alias Pina.3
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'INPUTS All Active Low
Col0 Alias Pinc.0
Col1 Alias Pinc.1                                           ' Keyboard Column
Col2 Alias Pinc.2
Col3 Alias Pinc.3
Col4 Alias Pinc.4
Col5 Alias Pinc.5


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Declare Sub Setup_keytable()
Declare Sub Setup_custom_key()
Declare Sub Add_key_buffer()
Declare Sub Remove_key_buffer()

Declare Sub Sendscankbd_release(byval Value As Byte)
Declare Sub Sendscankbd_press(byval Value As Byte)

' utility function
Declare Sub Hero_number_control()                           ' check hero use
Declare Sub Autoswap_keybuffer()                            ' for remove
Declare Sub Check_specialkey_action(byval Value As Byte)    ' check before send
Declare Sub Add_key_to_empty_slot()                         ' add keyvalue to keybuffer
Declare Function Check_exist_keybuffer() As Byte            ' check for exist key
Declare Function Check_bitvalue(byval Value As Byte) As Byte       ' check special bit at Key number
Declare Sub Set_number_bitkey(byval Value As Byte)
Declare Sub Reset_number_bitkey(byval Value As Byte)

Declare Sub Sendscankbd_dummy(byval Values As String)
Declare Sub Sendscankbd_word(byval Values As String)
Declare Sub Search_hero()                                   ' search hero in hero_number var
Declare Function Key2number(byval Values As String) As Byte ' change key character to number


' eeprom utility
Declare Sub Setdefault_keycode()
Declare Sub Setdefault_keytype()

' read one record from internal eeprom
' return hero id
' hero name save in strword max 30 byte last is 0
' skill 1 - 6 save in keybuffer
Declare Function Eeprom_search(byval Pos As Byte) As Byte
' write data to internal EEPROM
' hero name save in strWord
' skill 1 - 6 save in keybuffer
Declare Sub Eeprom_save(byval Pos As Byte , Byval Heroid As Byte)

' flag
Dim F_numlock As Bit
Dim F_specialkey_debounce As Bit
Dim F_show_team As Bit                                      ' comeback for show, what team for user uses
Dim F_ask_hero As Bit                                       ' ask user what's hero play ?
Dim F_search_hero As Bit
Dim F_ask_version As Bit

'Dim F_back2special As Bit                                   ' comeback to press again
'Dim F_debounce As Bit

Dim Specialkey_cnt As Byte
Dim Numlock_cnt As Byte
Dim Special_flag As Long                                    ' bit flag for special key (32)24-1
Dim Tmp_special_flag As Long                                ' mirror...

Dim Hero_number As Byte                                     ' hero use
' 10 - 19 = sentinel
' 20 - 29 = scourge
Dim Team_type As Byte                                       ' current user use sentinel or scourge
' 10 = sentinel 800 x 600
' 11 = sentinel 1024 x 768
' 12 = sentinel 1152 x 864
' 13 = sentinel 1280 x 1024
'
' 20 = scourge 800 x 600
' 21 = scourge 1024 x 768
' 22 = scourge 1152 x 864
' 23 = scourge 1280 x 1024


Dim Rowcount As Byte
Dim Colcount As Byte
Dim Keyvalue As Byte


Dim Strword As String * 30
Dim Tmp_str As String * 2                                   ' str command for send
Dim Tmp_str2 As String * 2                                  ' str command for send

Dim I As Byte , J As Byte , K As Byte
'Dim L As Byte , M As Byte , N As Byte                       ' reserve for use in timer0 interrupt

Dim Keybuffer(_max_buffer) As Byte                          ' keyboard buffer
Dim Keytable_code(_all_key) As Byte
Dim Keytable_type(_all_key) As Byte


'-------------- Main ----------------------------------------------------------

'configure PS2 pins
Enable Interrupts
Config Atemu = Int1 , Data = Pind.3 , Clock = Pinb.0
'                 ^------------------------ used interrupt
'                              ^----------- pin connected to DATA
'                                       ^-- pin connected to clock
'Note that the DATA must be connected to the used interrupt pin

'Config Timer0 = Timer , Prescale = 256                      ' 8.16ms @ 8Mhz
'Stop Timer0

Waitms 500                                                  ' optional delay

'Reset F_back2special
Reset F_numlock
'Debounce_cnt = 0


' setup default keycode and keytype
Call Setup_keytable()

' setup custom keycode and keytype
Call Setup_custom_key()

Print "*********************************"
Print "  " ; _program_name ; " " ; _firmware_version
Print "*********************************"


' Note : AVR
' Again, note that the AVR port pins have a data direction register
' when you want to use a pin as an input it must be set low first
' you can do this by writing zeros to the DDRx:
' DDRB =&B11110000  'this will set portb1.0,portb.1,portb.2 and portb.3 to use as inputs.
'
' So : when you want to use a pin as an input set it low first in the DDRx!
'     and read with PINx
'     and when you want to use the pin as output, write a 1 first
'     and write the value to PORTx

Config Porta = Output
Config Portc = Input

'Start Timer0
Do
   For Rowcount = 0 To 3
      Porta = &HFF
      Reset Porta.rowcount
      For Colcount = 0 To 5
         Keyvalue = 0
         Portc = &HFF
         Gosub Checkkey                                     ' check key for add or remove

         If Pinc.colcount = 0 Then
            Waitms _debounce
            ' add key to buffer
            If Pinc.colcount = 0 Then
               'print Str(keyvalue)
               Call Add_key_buffer
            End If
         Else
            ' remove key from buffer
            Call Remove_key_buffer
         End If
      Next Colcount
   Next Rowcount

   Gosub Timer0_service
Loop
'------------- make keypress value ---------------------------------------------
Checkkey:
   Keyvalue = Rowcount * 6
   Keyvalue = Keyvalue + Colcount
   Keyvalue = Keyvalue + 1
   ''print Str(keyvalue)
Return

'------------ Timer0 Service ---------------------------------------------------
' send data to PC
' do this every 8.16 ms
Timer0_service:
'   ' clear timer0 flag
'   Set Tifr.1

   ' numlock service
   If F_numlock = 1 And Numlock_cnt <= _numlock_wait Then
      Incr Numlock_cnt
      If Numlock_cnt >= _numlock_wait Then
         ' diable all special key before
         ' 0 = disable, 1 = enable
         Call Check_specialkey_action(0)

         '''''print "keypress : send num off"
         Sendscankbd Key_p_num
         Sendscankbd Key_r_num
'         ' if real PS/2 port must send twice
'         Sendscankbd Key_p_num
'         Sendscankbd Key_r_num

         ' reset value
         Reset F_numlock
         Numlock_cnt = 0

         ' enable special key again
         Call Check_specialkey_action(1)
      End If
   End If

   ' special key debounce
   If F_specialkey_debounce = 1 And Specialkey_cnt <= _specialkey_debounce Then
      Incr Specialkey_cnt
      If Specialkey_cnt >= _specialkey_debounce Then
         Reset F_specialkey_debounce
         Specialkey_cnt = 0
      End If
   End If

   ' set up team type
   If F_show_team = 1 Then
      ' when release all key then make action
      J = 0
      For I = 1 To _max_buffer
         If Keybuffer(i) > 0 Then J = 1
      Next I

      ' release all key..
      If J = 0 Then
         Reset F_show_team
         Strword = "Penter" : Call Sendscankbd_dummy(strword)
         Strword = "Renter" : Call Sendscankbd_dummy(strword)

         If Team_type >= 10 And Team_type <= 19 Then
            'print "sentinel"
            Strword = "You choose Sentinel " : Call Sendscankbd_word(strword)
         Else
            'print "scourge"
            Strword = "You choose Scourge " : Call Sendscankbd_word(strword)
         End If

         Select Case Team_type
            Case 10 : Strword = "800 x 600"
                        Call Sendscankbd_word(strword)
            Case 20 : Strword = "800 x 600"
                        Call Sendscankbd_word(strword)
            Case 11 : Strword = "1024 x 768"
                        Call Sendscankbd_word(strword)
            Case 21 : Strword = "1024 x 768"
                        Call Sendscankbd_word(strword)
            Case 12 : Strword = "1152 x 864"
                        Call Sendscankbd_word(strword)
            Case 22 : Strword = "1152 x 864"
                        Call Sendscankbd_word(strword)
            Case 13 : Strword = "1280 x 1024"
                        Call Sendscankbd_word(strword)
            Case 23 : Strword = "1280 x 1024"
                        Call Sendscankbd_word(strword)
         End Select

         Strword = "Penter" : Call Sendscankbd_dummy(strword)
         Strword = "Renter" : Call Sendscankbd_dummy(strword)
      End If
   End If

   ' ask hero
   If F_ask_hero = 1 Then
      ' when release all key then make action
      J = 0
      For I = 1 To _max_buffer
         If Keybuffer(i) > 0 Then J = 1
      Next I

      ' release all key..
      If J = 0 Then
         'print "flag hero = 1"
         Strword = "Penter" : Call Sendscankbd_dummy(strword)
         Strword = "Renter" : Call Sendscankbd_dummy(strword)
         Strword = "Type your Hero number.. " : Call Sendscankbd_word(strword)
         Call Hero_number_control()
      End If
   End If

   If F_ask_version = 1 Then
   ' when release all key then make action
      J = 0
      For I = 1 To _max_buffer
         If Keybuffer(i) > 0 Then J = 1
      Next I

      ' release all key..
      If J = 0 Then
         Reset F_ask_version
         'print _program_name ; "version " ; _firmware_version

         Strword = "Penter" : Call Sendscankbd_dummy(strword)
         Strword = "Renter" : Call Sendscankbd_dummy(strword)
         Strword = _program_name + " "
         Call Sendscankbd_word(strword)
         Strword = _firmware_version
         Call Sendscankbd_word(strword)
         Strword = "Penter" : Call Sendscankbd_dummy(strword)
         Strword = "Renter" : Call Sendscankbd_dummy(strword)
      End If
   End If

   If F_search_hero = 1 And F_numlock = 0 Then
      ' when release all key then make action
      J = 0
      For I = 1 To _max_buffer
         If Keybuffer(i) > 0 Then J = 1
      Next I

      ' release all key..
      If J = 0 Then
         ' clear flag
         Reset F_search_hero
         Call Search_hero()
      End If
   End If

Return

'------------- hero number for player ------------------------------------------

Sub Hero_number_control()
   Hero_number = 0
   Do
      For Rowcount = 0 To 3
         Porta = &HFF
         Reset Porta.rowcount
         For Colcount = 0 To 5
            Keyvalue = 0
            Portc = &HFF
            Gosub Checkkey                                  ' check key press

            If Pinc.colcount = 0 Then
               Waitms _debounce
               ' really press
               If Pinc.colcount = 0 Then
                  Do : Loop Until Pinc.colcount = 1         ' wait until release key
                  Waitms _debounce
                  If Pinc.colcount = 0 Then Goto End_check
                  'print Str(keyvalue)
                  Select Case Keyvalue
                     Case 1 : Strword = "P1"
                               Call Sendscankbd_dummy(strword)
                               Strword = "R1"
                               Call Sendscankbd_dummy(strword)
                               I = Hero_number * 10
                               Hero_number = I + 1
                     Case 2 : Strword = "P2"
                               Call Sendscankbd_dummy(strword)
                               Strword = "R2"
                               Call Sendscankbd_dummy(strword)
                               I = Hero_number * 10
                               Hero_number = I + 2
                     Case 3 : Strword = "P3"
                               Call Sendscankbd_dummy(strword)
                               Strword = "R3"
                               Call Sendscankbd_dummy(strword)
                               I = Hero_number * 10
                               Hero_number = I + 3
                     Case 7 : Strword = "P4"
                               Call Sendscankbd_dummy(strword)
                               Strword = "R4"
                               Call Sendscankbd_dummy(strword)
                               I = Hero_number * 10
                               Hero_number = I + 4
                     Case 8 : Strword = "P5"
                               Call Sendscankbd_dummy(strword)
                               Strword = "R5"
                               Call Sendscankbd_dummy(strword)
                               I = Hero_number * 10
                               Hero_number = I + 5
                     Case 9 : Strword = "P6"
                               Call Sendscankbd_dummy(strword)
                               Strword = "R6"
                               Call Sendscankbd_dummy(strword)
                               I = Hero_number * 10
                               Hero_number = I + 6
                     Case 13 : Strword = "P7"
                               Call Sendscankbd_dummy(strword)
                               Strword = "R7"
                               Call Sendscankbd_dummy(strword)
                               I = Hero_number * 10
                               Hero_number = I + 7
                     Case 14 : Strword = "P8"
                               Call Sendscankbd_dummy(strword)
                               Strword = "R8"
                               Call Sendscankbd_dummy(strword)
                               I = Hero_number * 10
                               Hero_number = I + 8
                     Case 15 : Strword = "P9"
                               Call Sendscankbd_dummy(strword)
                               Strword = "R9"
                               Call Sendscankbd_dummy(strword)
                               I = Hero_number * 10
                               Hero_number = I + 9
                     Case 19 : Strword = "Pesc"
                               Call Sendscankbd_dummy(strword)
                               Strword = "Resc"
                               Call Sendscankbd_dummy(strword)
                               'reset hero number
                               Reset F_ask_hero
                               Hero_number = 0

                     Case 20 : Strword = "P0"
                               Call Sendscankbd_dummy(strword)
                               Strword = "R0"
                               Call Sendscankbd_dummy(strword)
                               I = Hero_number * 10
                               Hero_number = I
                     Case 21 : Strword = "Penter"
                               Call Sendscankbd_dummy(strword)
                               Strword = "Renter"
                               Call Sendscankbd_dummy(strword)
                               ' reset flag
                               Reset F_ask_hero
                               ' search hero
                               'print "hero number " ; Hero_number
                               'Call Search_hero()
                               Set F_search_hero
                  End Select
               End If
            End If
         Next Colcount
      Next Rowcount
End_check:
   Loop Until F_ask_hero = 0

End Sub

'------------- eeprom read -----------------------------------------------------
' return hero id
Function Eeprom_search(byval Pos As Byte) As Byte
   Local Addr As Word

   Addr = Pos - 1
   Addr = Addr * 38
   Addr = Addr + _adr_custom_hero
   Readeeprom J , Addr
   ''print "Hero id = " ; J

   Incr Addr : Readeeprom Keybuffer(1) , Addr
   Incr Addr : Readeeprom Keybuffer(2) , Addr
   Incr Addr : Readeeprom Keybuffer(3) , Addr
   Incr Addr : Readeeprom Keybuffer(4) , Addr
   Incr Addr : Readeeprom Keybuffer(5) , Addr
   Incr Addr : Readeeprom Keybuffer(6) , Addr
   Incr Addr : Readeeprom Strword , Addr

   Eeprom_search = J
End Function

'------------- eeprom write ----------------------------------------------------
Sub Eeprom_save(byval Pos As Byte , Byval Heroid As Byte)
   Local Addr As Word
   Local Count As Byte
   Local Tmp_chr As String * 1

   ' formula = (pos - 1) * 38 +1
   Addr = Pos - 1
   Addr = Addr * 38
   Addr = Addr + _adr_custom_hero

   Writeeeprom Heroid , Addr                                'hero id

   Incr Addr : Writeeeprom Keybuffer(1) , Addr
   Incr Addr : Writeeeprom Keybuffer(2) , Addr
   Incr Addr : Writeeeprom Keybuffer(3) , Addr
   Incr Addr : Writeeeprom Keybuffer(4) , Addr
   Incr Addr : Writeeeprom Keybuffer(5) , Addr
   Incr Addr : Writeeeprom Keybuffer(6) , Addr

   Count = Len(strword)
   Count = 30 - Count
   Strword = Strword + Space(count)
   Incr Addr : Writeeeprom Strword , Addr
End Sub
'------------- search hero -----------------------------------------------------
Sub Search_hero()
   Local Addr As Word
   Local Pos As Byte

   Strword = "Penter"
   Call Sendscankbd_dummy(strword)
   Strword = "Renter"
   Call Sendscankbd_dummy(strword)

   ' important !! dont forget clear keybuffer
   For J = 1 To _max_buffer
      Keybuffer(j) = 0
   Next J


   ' search in eeprom first
   ' if found go to end
   I = 0
   Pos = 0                                                  ' record position
   Do
      Incr Pos
      I = Eeprom_search(pos)
   Loop Until I = Hero_number Or I = 255 Or Pos > 20        ' only 20 record

   ' found hero
   If I = Hero_number Then
      Strword = Trim(strword)
      Call Sendscankbd_word(strword)
      Strword = " is your hero"
      Call Sendscankbd_word(strword)

      'set new key to board
      For I = 1 To _all_key
         Select Case Keytable_type(i)
         Case 25 : Keytable_code(i) = Keybuffer(1)
         Case 26 : Keytable_code(i) = Keybuffer(2)
         Case 27 : Keytable_code(i) = Keybuffer(3)
         Case 28 : Keytable_code(i) = Keybuffer(4)
         Case 29 : Keytable_code(i) = Keybuffer(5)
         Case 30 : Keytable_code(i) = Keybuffer(6)
         End Select
      Next I

      Goto End_search
   End If

   ' not found in EEPROM , search in default code
   Select Case Hero_number
      Case 1 To 16 : Restore Group_1
      Case 17 To 32 : Restore Group_2
      Case 33 To 48 : Restore Group_3
      Case 49 To 64 : Restore Group_4
      Case 65 To 82 : Restore Group_5
      Case Else : Restore Group_6
   End Select


   I = 0
   Do
      Read I                                                ' hero number
      Read Strword                                          ' hero name
      ''print I ; Strword
      Read K                                                ' all skill
      For J = 1 To K
         Read Tmp_str
         Keybuffer(j) = Key2number(tmp_str)
      Next J
   Loop Until I = Hero_number Or I = 255

   If I = Hero_number Then
      ' found
      Call Sendscankbd_word(strword)
      Strword = " is your hero"
      Call Sendscankbd_word(strword)

      'set new key to board
      For I = 1 To _all_key
         Select Case Keytable_type(i)
         Case 25 : Keytable_code(i) = Keybuffer(1)
         Case 26 : Keytable_code(i) = Keybuffer(2)
         Case 27 : Keytable_code(i) = Keybuffer(3)
         Case 28 : Keytable_code(i) = Keybuffer(4)
         Case 29 : Keytable_code(i) = Keybuffer(5)
         Case 30 : Keytable_code(i) = Keybuffer(6)
         End Select
      Next I

      Goto End_search
   End If


   If I = 255 Then
      'print "not found"
      Strword = "Hero not Found.., try again.."
      Call Sendscankbd_word(strword)
   End If


End_search:
   Strword = "Penter"
   Call Sendscankbd_dummy(strword)
   Strword = "Renter"
   Call Sendscankbd_dummy(strword)

   ' important !! dont forget clear keybuffer
   For J = 1 To _max_buffer
      Keybuffer(j) = 0
   Next J

End Sub
'------------- load data and setup key table -----------------------------------
Sub Setup_keytable
   Restore Keytable_type
   For I = 1 To _all_key
      Read Keytable_type(i)
      '''''print Keytable_type(i)
   Next I

   Restore Keytable_code
   For I = 1 To _all_key
      Read Keytable_code(i)
      '''''print Keytable_code(i)
   Next I
End Sub

'------------- load data and setup key table from EEPROM -----------------------
Sub Setup_custom_key()
   Local Addr As Word
   Local Adr_incr As Byte
   Local Tmp_code As Byte

   ' if data = 0 , that data will cross over

   ' read key code
   For Adr_incr = 1 To _all_key
      Addr = _adr_custom_key_code + Adr_incr
      Addr = Addr - 1
      Readeeprom Tmp_code , Addr
      If Tmp_code <> 0 And Tmp_code <> 255 Then Keytable_code(adr_incr) = Tmp_code
   Next Adr_incr

   ' read key type
   For Adr_incr = 1 To _all_key
      Addr = _adr_custom_key_type + Adr_incr
      Addr = Addr - 1
      Readeeprom Tmp_code , Addr
      If Tmp_code <> 0 And Tmp_code <> 255 Then Keytable_type(adr_incr) = Tmp_code
   Next Adr_incr
End Sub

'------------ auto swap empty key buffer ---------------------------------------
Sub Autoswap_keybuffer()
   For K = 1 To _dec_max_buffer
      If Keybuffer(k) <= 0 Then
         Swap Keybuffer(k) , Keybuffer(k + 1)
      End If
   Next
End Sub

'------------- check status of special bit each numer key ----------------------
' passed
Function Check_bitvalue(byval Value As Byte) As Byte        ' check special bit at Key number
   Tmp_special_flag = Special_flag
   J = Value - 1
   Shift Tmp_special_flag , Right , J
   Tmp_special_flag = Tmp_special_flag And &H0001
   Check_bitvalue = Tmp_special_flag
End Function
'------------- set special bit key --------------------------------------------
' passed
Sub Set_number_bitkey(byval Value As Byte)
   Tmp_special_flag = &H0001
   J = Value - 1
   Shift Tmp_special_flag , Left , J
   Special_flag = Special_flag Or Tmp_special_flag
End Sub
'------------- reset special bit key --------------------------------------------
' passed
Sub Reset_number_bitkey(byval Value As Byte)
   Tmp_special_flag = &H0001
   J = Value - 1
   Shift Tmp_special_flag , Left , J
   Tmp_special_flag = Tmp_special_flag Xor &HFFFFFFFF
   Special_flag = Special_flag And Tmp_special_flag
End Sub
'------------- remove key from buffer ------------------------------------------
Sub Remove_key_buffer
   ' do..loop count = 0 to _all_key (check all key in key buffer)
   I = 0
   While I < _max_buffer
      Incr I
      ' if keybuffer(i) = keyvalue (that key is release must remove)
      If Keybuffer(i) = Keyvalue Then

         ' not special key , check and sendscankbd release key
         If Keytable_type(keyvalue) <> 3 Then
            J = Keytable_code(keybuffer(i))
            Call Sendscankbd_release(j)
            ' keybuffer(count) = 0
            Keybuffer(i) = 0
            ' swap next key to remove key and swap next next next until end buffer
            Call Autoswap_keybuffer()
         End If

      End If
   Wend
End Sub
' ========================================================================
' check with PB .. pass
Function Check_exist_keybuffer() As Byte
   J = 0                                                    ' false
   I = 0
   While I < _max_buffer And J = 0
      Incr I
      If Keybuffer(i) = Keyvalue Then
         J = I
      End If
   Wend
   Check_exist_keybuffer = J
End Function

'------------ add key to empty slot --------------------------------------------
Sub Add_key_to_empty_slot()
   I = 0
   While I < _max_buffer
      Incr I
      If Keybuffer(i) <= 0 Then
         Keybuffer(i) = Keyvalue
         I = _max_buffer
      End If
   Wend


   '
   ' check hero use
   '
   If Keybuffer(1) = 19 And Keybuffer(2) = 21 Then
      Set F_ask_hero
      Hero_number = 0
   Elseif Keybuffer(1) = 21 And Keybuffer(2) = 19 Then
      Set F_ask_version
   '
   ' cmd for start using keyboard
   '
   ' press button 1 - 4 play sentinel 800 x 600
   Elseif Keybuffer(1) = 1 And Keybuffer(2) = 2 And Keybuffer(3) = 3 And Keybuffer(4) = 4 Then
      ' check at timer service when data empty then 'print "you choose sentinel" to screen
      Set F_show_team                                       ' back for show text to screen
      Team_type = 10
   ' press button 7 - 10 play sentinel 1024 x 768
   Elseif Keybuffer(1) = 7 And Keybuffer(2) = 8 And Keybuffer(3) = 9 And Keybuffer(4) = 10 Then
      ' check at timer service when data empty then 'print "you choose sentinel" to screen
      Set F_show_team                                       ' back for show text to screen
      Team_type = 11
   ' press button 13 - 16 play sentinel 1152 x 864
   Elseif Keybuffer(1) = 13 And Keybuffer(2) = 14 And Keybuffer(3) = 15 And Keybuffer(4) = 16 Then
      ' check at timer service when data empty then 'print "you choose sentinel" to screen
      Set F_show_team                                       ' back for show text to screen
      Team_type = 12
   ' press button 19 - 22 play sentinel 1280 x 1024
   Elseif Keybuffer(1) = 19 And Keybuffer(2) = 20 And Keybuffer(3) = 21 And Keybuffer(4) = 22 Then
      ' check at timer service when data empty then 'print "you choose sentinel" to screen
      Set F_show_team                                       ' back for show text to screen
      Team_type = 13

   ' press button 1 - 4 play scourge 800 x 600
   Elseif Keybuffer(1) = 4 And Keybuffer(2) = 3 And Keybuffer(3) = 2 And Keybuffer(4) = 1 Then
      ' check at timer service when data empty then 'print "you choose sentinel" to screen
      Set F_show_team
      Team_type = 20
   ' press button 22 - 19 play scourge 1024 x 768
   Elseif Keybuffer(1) = 10 And Keybuffer(2) = 9 And Keybuffer(3) = 8 And Keybuffer(4) = 7 Then
      ' check at timer service when data empty then 'print "you choose sentinel" to screen
      Set F_show_team
      Team_type = 21
   ' press button 13 - 16 play scourge 1152 x 864
   Elseif Keybuffer(1) = 16 And Keybuffer(2) = 15 And Keybuffer(3) = 14 And Keybuffer(4) = 13 Then
      ' check at timer service when data empty then 'print "you choose sentinel" to screen
      Set F_show_team
      Team_type = 22
   ' press button 22 - 19 play scourge
   Elseif Keybuffer(1) = 22 And Keybuffer(2) = 21 And Keybuffer(3) = 20 And Keybuffer(4) = 19 Then
      ' check at timer service when data empty then 'print "you choose sentinel" to screen
      Set F_show_team
      Team_type = 23
   End If
End Sub

'------------ check special key and enable or disable --------------------------
Sub Check_specialkey_action(byval Value As Byte)
   For I = 1 To _max_buffer
      If Keytable_type(keybuffer(i)) = 3 Then
         J = Keytable_code(keybuffer(i))
         ' value = 0, disable
         If Value = 0 Then
            Call Sendscankbd_release(j)
         Else
         ' enable
            Call Sendscankbd_press(j)
         End If
      End If
   Next I
End Sub

'------------ check key value and show -----------------------------------------
' Add ID Key from Key Buffer
Sub Add_key_buffer
   '
   ' special key , control by myself
   If Keytable_type(keyvalue) = 3 Then

      If F_specialkey_debounce = 0 Then

         'set flag for wait some ms
         Set F_specialkey_debounce

         'print "==== 1 ===== flag " ; Bin(special_flag)
         ' if flag clear
         If Check_bitvalue(keyvalue) = 0 Then
            'print "special key press" ; Keyvalue
            ' send key
            Call Sendscankbd_press(keytable_code(keyvalue))
            ' set flag
            Call Set_number_bitkey(keyvalue)
            ' add special key to slot
            If Check_exist_keybuffer() = 0 Then
               Call Add_key_to_empty_slot()
            End If
         Else
            ' if flag set
            'print "special key release" ; Keyvalue
            ' send key
            Call Sendscankbd_release(keytable_code(keyvalue))
            ' clear flag
            Call Reset_number_bitkey(keyvalue)
            ' clear buffer
            K = Check_exist_keybuffer()
            Keybuffer(k) = 0
            ' swap next key to remove key and swap next next next until end buffer
            Call Autoswap_keybuffer()
         End If

         'print "==== 2 ===== flag " ; Bin(special_flag)

      End If

      Goto End_addkey
   End If
'

   ' check exist key , if = 0 is not exist
   If Check_exist_keybuffer() = 0 Then
      '
      ' check key type...
      ' if normal key or skill key
      If Keytable_type(keyvalue) = 1 Or Keytable_type(keyvalue) >= 25 Then
         ' diable all special key before
         ' 0 = disable, 1 = enable
         Call Check_specialkey_action(0)
         ' send pc key
         Call Sendscankbd_press(keytable_code(keyvalue))
'         'Call Sendscankbd_release(keytable_code(keyvalue))
         ' enable special key after
         Call Check_specialkey_action(1)
      ' else if numeric key then
      Elseif Keytable_type(keyvalue) = 2 Then
         ' diable all special key before
         ' 0 = disable, 1 = enable
         Call Check_specialkey_action(0)
         ' check numlock flag, now numlock is on/off ?
         If F_numlock = 0 Then
            ' set flag for back numlock off
            Set F_numlock
            ' send numlock on
            Sendscankbd Key_p_num
            Sendscankbd Key_r_num
'         ' if real PS/2 port must send twice
'         Sendscankbd Key_p_num
'         Sendscankbd Key_r_num
         End If
         ' reset time counter for flag number
         Numlock_cnt = 0
         ' send numeric key

         Call Sendscankbd_press(keytable_code(keyvalue))
'         Call Sendscankbd_release(keytable_code(keyvalue))
         ' enable special key
         Call Check_specialkey_action(1)
      ' combo key
      Elseif Keytable_type(keyvalue) = 3 Then
         '
         'Call Sendscankbd_press(keytable_code(keyvalue))
         '
      Elseif Keytable_type(keyvalue) = 4 Then
         ' disable all special key
         ' xxxx
         ' enable special key
      End If

      ' if not pause key, add keyvalue to empty slot
      If Keytable_code(keyvalue) <> 70 Then
         Call Add_key_to_empty_slot()
      End If
   End If

End_addkey:
End Sub

'---------- send key to pc -----------------------
' p = pressed
' r = release
'
Sub Sendscankbd_release(byval Value As Byte)
   Select Case Value
      Case 1 : Sendscankbd Key_r_a
      Case 2 : Sendscankbd Key_r_b
      Case 3 : Sendscankbd Key_r_c
      Case 4 : Sendscankbd Key_r_d
      Case 5 : Sendscankbd Key_r_e
      Case 6 : Sendscankbd Key_r_f
      Case 7 : Sendscankbd Key_r_g
      Case 8 : Sendscankbd Key_r_h
      Case 9 : Sendscankbd Key_r_i
      Case 10 : Sendscankbd Key_r_j
      Case 11 : Sendscankbd Key_r_k
      Case 12 : Sendscankbd Key_r_l
      Case 13 : Sendscankbd Key_r_m
      Case 14 : Sendscankbd Key_r_n
      Case 15 : Sendscankbd Key_r_o
      Case 16 : Sendscankbd Key_r_p
      Case 17 : Sendscankbd Key_r_q
      Case 18 : Sendscankbd Key_r_r
      Case 19 : Sendscankbd Key_r_s
      Case 20 : Sendscankbd Key_r_t
      Case 21 : Sendscankbd Key_r_u
      Case 22 : Sendscankbd Key_r_v
      Case 23 : Sendscankbd Key_r_w
      Case 24 : Sendscankbd Key_r_x
      Case 25 : Sendscankbd Key_r_y
      Case 26 : Sendscankbd Key_r_z
      Case 27 : Sendscankbd Key_r_0
      Case 28 : Sendscankbd Key_r_1
      Case 29 : Sendscankbd Key_r_2
      Case 30 : Sendscankbd Key_r_3
      Case 31 : Sendscankbd Key_r_4
      Case 32 : Sendscankbd Key_r_5
      Case 33 : Sendscankbd Key_r_6
      Case 34 : Sendscankbd Key_r_7
      Case 35 : Sendscankbd Key_r_8
      Case 36 : Sendscankbd Key_r_9
      Case 37 : Sendscankbd Key_r_backqoute
      Case 38 : Sendscankbd Key_r_dash
      Case 39 : Sendscankbd Key_r_equal
      Case 40 : Sendscankbd Key_r_backslash
      Case 41 : Sendscankbd Key_r_bksp
      Case 42 : Sendscankbd Key_r_space
      Case 43 : Sendscankbd Key_r_tab
      Case 44 : Sendscankbd Key_r_caps
      Case 45 : Sendscankbd Key_r_lshft
      Case 46 : Sendscankbd Key_r_lctrl
      Case 47 : Sendscankbd Key_r_lgui
      Case 48 : Sendscankbd Key_r_lalt
      Case 49 : Sendscankbd Key_r_rshft
      Case 50 : Sendscankbd Key_r_rctrl
      Case 51 : Sendscankbd Key_r_rgui
      Case 52 : Sendscankbd Key_r_ralt
      Case 53 : Sendscankbd Key_r_apps
      Case 54 : Sendscankbd Key_r_enter
      Case 55 : Sendscankbd Key_r_esc
      Case 56 : Sendscankbd Key_r_f1
      Case 57 : Sendscankbd Key_r_f2
      Case 58 : Sendscankbd Key_r_f3
      Case 59 : Sendscankbd Key_r_f4
      Case 60 : Sendscankbd Key_r_f5
      Case 61 : Sendscankbd Key_r_f6
      Case 62 : Sendscankbd Key_r_f7
      Case 63 : Sendscankbd Key_r_f8
      Case 64 : Sendscankbd Key_r_f9
      Case 65 : Sendscankbd Key_r_f10
      Case 66 : Sendscankbd Key_r_f11
      Case 67 : Sendscankbd Key_r_f12
      Case 68 : Sendscankbd Key_r_prnscrn
      Case 69 : Sendscankbd Key_r_scroll
      'case 70 : SendScankbd key_r_PAUSE  ' this key dont have release code
      Case 71 : Sendscankbd Key_r_openbrace
      Case 72 : Sendscankbd Key_r_insert
      Case 73 : Sendscankbd Key_r_home
      Case 74 : Sendscankbd Key_r_pgup
      Case 75 : Sendscankbd Key_r_delete
      Case 76 : Sendscankbd Key_r_end
      Case 77 : Sendscankbd Key_r_pgdn
      Case 78 : Sendscankbd Key_r_uarrow
      Case 79 : Sendscankbd Key_r_larrow
      Case 80 : Sendscankbd Key_r_darrow
      Case 81 : Sendscankbd Key_r_rarrow
      Case 82 : Sendscankbd Key_r_num
      Case 83 : Sendscankbd Key_r_kpslash
      Case 84 : Sendscankbd Key_r_kpstar
      Case 85 : Sendscankbd Key_r_kpdash
      Case 86 : Sendscankbd Key_r_kpplus
      Case 87 : Sendscankbd Key_r_kpen
      Case 88 : Sendscankbd Key_r_kpdot
      Case 89 : Sendscankbd Key_r_kp0
      Case 90 : Sendscankbd Key_r_kp1
      Case 91 : Sendscankbd Key_r_kp2
      Case 92 : Sendscankbd Key_r_kp3
      Case 93 : Sendscankbd Key_r_kp4
      Case 94 : Sendscankbd Key_r_kp5
      Case 95 : Sendscankbd Key_r_kp6
      Case 96 : Sendscankbd Key_r_kp7
      Case 97 : Sendscankbd Key_r_kp8
      Case 98 : Sendscankbd Key_r_kp9
      Case 99 : Sendscankbd Key_r_closebrace
      Case 100 : Sendscankbd Key_r_semicolon
      Case 101 : Sendscankbd Key_r_qoute
      Case 102 : Sendscankbd Key_r_comma
      Case 103 : Sendscankbd Key_r_dot
      Case 104 : Sendscankbd Key_r_slash
      Case 105 : Sendscankbd Key_r_power
      Case 106 : Sendscankbd Key_r_sleep
      Case 107 : Sendscankbd Key_r_wake
      Case 108 : Sendscankbd Key_r_next_track
      Case 109 : Sendscankbd Key_r_previous_track
      Case 110 : Sendscankbd Key_r_stop
      Case 111 : Sendscankbd Key_r_play_pause
      Case 112 : Sendscankbd Key_r_mute
      Case 113 : Sendscankbd Key_r_volume_up
      Case 114 : Sendscankbd Key_r_volume_down
      Case 115 : Sendscankbd Key_r_media_select
      Case 116 : Sendscankbd Key_r_e_mail
      Case 117 : Sendscankbd Key_r_calculator
      Case 118 : Sendscankbd Key_r_my_computer
      Case 119 : Sendscankbd Key_r_www_search
      Case 120 : Sendscankbd Key_r_www_home
      Case 121 : Sendscankbd Key_r_www_back
      Case 122 : Sendscankbd Key_r_www_forward
      Case 123 : Sendscankbd Key_r_www_stop
      Case 124 : Sendscankbd Key_r_www_refresh
      Case 125 : Sendscankbd Key_r_www_favorites
   End Select
End Sub


'============ send key press
Sub Sendscankbd_press(byval Value As Byte)
   Select Case Value
      Case 1 : Sendscankbd Key_p_a
      Case 2 : Sendscankbd Key_p_b
      Case 3 : Sendscankbd Key_p_c
      Case 4 : Sendscankbd Key_p_d
      Case 5 : Sendscankbd Key_p_e
      Case 6 : Sendscankbd Key_p_f
      Case 7 : Sendscankbd Key_p_g
      Case 8 : Sendscankbd Key_p_h
      Case 9 : Sendscankbd Key_p_i
      Case 10 : Sendscankbd Key_p_j
      Case 11 : Sendscankbd Key_p_k
      Case 12 : Sendscankbd Key_p_l
      Case 13 : Sendscankbd Key_p_m
      Case 14 : Sendscankbd Key_p_n
      Case 15 : Sendscankbd Key_p_o
      Case 16 : Sendscankbd Key_p_p
      Case 17 : Sendscankbd Key_p_q
      Case 18 : Sendscankbd Key_p_r
      Case 19 : Sendscankbd Key_p_s
      Case 20 : Sendscankbd Key_p_t
      Case 21 : Sendscankbd Key_p_u
      Case 22 : Sendscankbd Key_p_v
      Case 23 : Sendscankbd Key_p_w
      Case 24 : Sendscankbd Key_p_x
      Case 25 : Sendscankbd Key_p_y
      Case 26 : Sendscankbd Key_p_z
      Case 27 : Sendscankbd Key_p_0
      Case 28 : Sendscankbd Key_p_1
      Case 29 : Sendscankbd Key_p_2
      Case 30 : Sendscankbd Key_p_3
      Case 31 : Sendscankbd Key_p_4
      Case 32 : Sendscankbd Key_p_5
      Case 33 : Sendscankbd Key_p_6
      Case 34 : Sendscankbd Key_p_7
      Case 35 : Sendscankbd Key_p_8
      Case 36 : Sendscankbd Key_p_9
      Case 37 : Sendscankbd Key_p_backqoute
      Case 38 : Sendscankbd Key_p_dash
      Case 39 : Sendscankbd Key_p_equal
      Case 40 : Sendscankbd Key_p_backslash
      Case 41 : Sendscankbd Key_p_bksp
      Case 42 : Sendscankbd Key_p_space
      Case 43 : Sendscankbd Key_p_tab
      Case 44 : Sendscankbd Key_p_caps
      Case 45 : Sendscankbd Key_p_lshft
      Case 46 : Sendscankbd Key_p_lctrl
      Case 47 : Sendscankbd Key_p_lgui
      Case 48 : Sendscankbd Key_p_lalt
      Case 49 : Sendscankbd Key_p_rshft
      Case 50 : Sendscankbd Key_p_rctrl
      Case 51 : Sendscankbd Key_p_rgui
      Case 52 : Sendscankbd Key_p_ralt
      Case 53 : Sendscankbd Key_p_apps
      Case 54 : Sendscankbd Key_p_enter
      Case 55 : Sendscankbd Key_p_esc
      Case 56 : Sendscankbd Key_p_f1
      Case 57 : Sendscankbd Key_p_f2
      Case 58 : Sendscankbd Key_p_f3
      Case 59 : Sendscankbd Key_p_f4
      Case 60 : Sendscankbd Key_p_f5
      Case 61 : Sendscankbd Key_p_f6
      Case 62 : Sendscankbd Key_p_f7
      Case 63 : Sendscankbd Key_p_f8
      Case 64 : Sendscankbd Key_p_f9
      Case 65 : Sendscankbd Key_p_f10
      Case 66 : Sendscankbd Key_p_f11
      Case 67 : Sendscankbd Key_p_f12
      Case 68 : Sendscankbd Key_p_prnscrn
      Case 69 : Sendscankbd Key_p_scroll
      Case 70 : Sendscankbd Key_p_pause
      Case 71 : Sendscankbd Key_p_openbrace
      Case 72 : Sendscankbd Key_p_insert
      Case 73 : Sendscankbd Key_p_home
      Case 74 : Sendscankbd Key_p_pgup
      Case 75 : Sendscankbd Key_p_delete
      Case 76 : Sendscankbd Key_p_end
      Case 77 : Sendscankbd Key_p_pgdn
      Case 78 : Sendscankbd Key_p_uarrow
      Case 79 : Sendscankbd Key_p_larrow
      Case 80 : Sendscankbd Key_p_darrow
      Case 81 : Sendscankbd Key_p_rarrow
      Case 82 : Sendscankbd Key_p_num
      Case 83 : Sendscankbd Key_p_kpslash
      Case 84 : Sendscankbd Key_p_kpstar
      Case 85 : Sendscankbd Key_p_kpdash
      Case 86 : Sendscankbd Key_p_kpplus
      Case 87 : Sendscankbd Key_p_kpen
      Case 88 : Sendscankbd Key_p_kpdot
      Case 89 : Sendscankbd Key_p_kp0
      Case 90 : Sendscankbd Key_p_kp1
      Case 91 : Sendscankbd Key_p_kp2
      Case 92 : Sendscankbd Key_p_kp3
      Case 93 : Sendscankbd Key_p_kp4
      Case 94 : Sendscankbd Key_p_kp5
      Case 95 : Sendscankbd Key_p_kp6
      Case 96 : Sendscankbd Key_p_kp7
      Case 97 : Sendscankbd Key_p_kp8
      Case 98 : Sendscankbd Key_p_kp9
      Case 99 : Sendscankbd Key_p_closebrace
      Case 100 : Sendscankbd Key_p_semicolon
      Case 101 : Sendscankbd Key_p_qoute
      Case 102 : Sendscankbd Key_p_comma
      Case 103 : Sendscankbd Key_p_dot
      Case 104 : Sendscankbd Key_p_slash
      Case 105 : Sendscankbd Key_p_power
      Case 106 : Sendscankbd Key_p_sleep
      Case 107 : Sendscankbd Key_p_wake
      Case 108 : Sendscankbd Key_p_next_track
      Case 109 : Sendscankbd Key_p_previous_track
      Case 110 : Sendscankbd Key_p_stop
      Case 111 : Sendscankbd Key_p_play_pause
      Case 112 : Sendscankbd Key_p_mute
      Case 113 : Sendscankbd Key_p_volume_up
      Case 114 : Sendscankbd Key_p_volume_down
      Case 115 : Sendscankbd Key_p_media_select
      Case 116 : Sendscankbd Key_p_e_mail
      Case 117 : Sendscankbd Key_p_calculator
      Case 118 : Sendscankbd Key_p_my_computer
      Case 119 : Sendscankbd Key_p_www_search
      Case 120 : Sendscankbd Key_p_www_home
      Case 121 : Sendscankbd Key_p_www_back
      Case 122 : Sendscankbd Key_p_www_forward
      Case 123 : Sendscankbd Key_p_www_stop
      Case 124 : Sendscankbd Key_p_www_refresh
      Case 125 : Sendscankbd Key_p_www_favorites
   End Select
End Sub

'=========== easy to send pc key ===============================================
Sub Sendscankbd_word(byval Values As String)

   ''print Values
   For K = 1 To Len(values)
      Tmp_str = Mid(values , K , 1)
      If Tmp_str = " " Then
         Call Sendscankbd_dummy( "Pspace")
         Call Sendscankbd_dummy( "Rspace")
         Goto Next_char
      End If

      I = Asc(tmp_str)
'         'print "i = " ; I
      If I >= 65 And I <= 90 Then
         ' press shift
         ''print "Pl shft " ; I
          Call Sendscankbd_dummy( "Pl shft")
      End If

      Tmp_str2 = "P" + Lcase(tmp_str)
      Call Sendscankbd_dummy(tmp_str2)
      Tmp_str2 = "R" + Lcase(tmp_str)
      Call Sendscankbd_dummy(tmp_str2)

      If I >= 65 And I <= 90 Then
         ' press shift
         ''print "Pl shft " ; I
          Call Sendscankbd_dummy( "Rl shft")
      End If

Next_char:
   Next K
End Sub
'=========== easy to send pc key ===============================================
Sub Sendscankbd_dummy(byval Values As String)

   Select Case Values
      Case "Pa" : Sendscankbd Key_p_a
      Case "Ra" : Sendscankbd Key_r_a
      Case "P9" : Sendscankbd Key_p_9
      Case "R9" : Sendscankbd Key_r_9
      Case "P[" : Sendscankbd Key_p_openbrace
      Case "R[" : Sendscankbd Key_r_openbrace
      Case "Pb" : Sendscankbd Key_p_b
      Case "Rb" : Sendscankbd Key_r_b
      Case "P`" : Sendscankbd Key_p_backqoute
      Case "R`" : Sendscankbd Key_r_backqoute
      Case "Pinsert" : Sendscankbd Key_p_insert
      Case "Rinsert" : Sendscankbd Key_r_insert
      Case "Pc" : Sendscankbd Key_p_c
      Case "Rc" : Sendscankbd Key_r_c
      Case "P-" : Sendscankbd Key_p_dash
      Case "R-" : Sendscankbd Key_r_dash
      Case "Phome" : Sendscankbd Key_p_home
      Case "Rhome" : Sendscankbd Key_r_home
      Case "Pd" : Sendscankbd Key_p_d
      Case "Rd" : Sendscankbd Key_r_d
      Case "P=" : Sendscankbd Key_p_equal
      Case "R=" : Sendscankbd Key_r_equal
      Case "Ppg up" : Sendscankbd Key_p_pgup
      Case "Rpg up" : Sendscankbd Key_r_pgup
      Case "Pe" : Sendscankbd Key_p_e
      Case "Re" : Sendscankbd Key_r_e
      Case "P\" : Sendscankbd Key_p_backslash
      Case "R\" : Sendscankbd Key_r_backslash
      Case "Pdelete" : Sendscankbd Key_p_delete
      Case "Rdelete" : Sendscankbd Key_r_delete
      Case "Pf" : Sendscankbd Key_p_f
      Case "Rf" : Sendscankbd Key_r_f
      Case "Pbksp" : Sendscankbd Key_p_bksp
      Case "Rbksp" : Sendscankbd Key_r_bksp
      Case "Pend" : Sendscankbd Key_p_end
      Case "Rend" : Sendscankbd Key_r_end
      Case "Pg" : Sendscankbd Key_p_g
      Case "Rg" : Sendscankbd Key_r_g
      Case "Pspace" : Sendscankbd Key_p_space
      Case "Rspace" : Sendscankbd Key_r_space
      Case "Ppg dn" : Sendscankbd Key_p_pgdn
      Case "Rpg dn" : Sendscankbd Key_r_pgdn
      Case "Ph" : Sendscankbd Key_p_h
      Case "Rh" : Sendscankbd Key_r_h
      Case "Ptab" : Sendscankbd Key_p_tab
      Case "Rtab" : Sendscankbd Key_r_tab
      Case "Pu arrow" : Sendscankbd Key_p_uarrow
      Case "Ru arrow" : Sendscankbd Key_r_uarrow
      Case "Pi" : Sendscankbd Key_p_i
      Case "Ri" : Sendscankbd Key_r_i
      Case "Pcaps" : Sendscankbd Key_p_caps
      Case "Rcaps" : Sendscankbd Key_r_caps
      Case "Pl arrow" : Sendscankbd Key_p_larrow
      Case "Rl arrow" : Sendscankbd Key_r_larrow
      Case "Pj" : Sendscankbd Key_p_j
      Case "Rj" : Sendscankbd Key_r_j
      Case "Pl shft" : Sendscankbd Key_p_lshft
      Case "Rl shft" : Sendscankbd Key_r_lshft
      Case "Pd arrow" : Sendscankbd Key_p_darrow
      Case "Rd arrow" : Sendscankbd Key_r_darrow
      Case "Pk" : Sendscankbd Key_p_k
      Case "Rk" : Sendscankbd Key_r_k
      Case "Pl ctrl" : Sendscankbd Key_p_lctrl
      Case "Rl ctrl" : Sendscankbd Key_r_lctrl
      Case "Pr arrow" : Sendscankbd Key_p_rarrow
      Case "Rr arrow" : Sendscankbd Key_r_rarrow
      Case "Pl" : Sendscankbd Key_p_l
      Case "Rl" : Sendscankbd Key_r_l
      Case "Pl gui" : Sendscankbd Key_p_lgui
      Case "Rl gui" : Sendscankbd Key_r_lgui
      Case "Pnum" : Sendscankbd Key_p_num
      Case "Rnum" : Sendscankbd Key_r_num
      Case "Pm" : Sendscankbd Key_p_m
      Case "Rm" : Sendscankbd Key_r_m
      Case "Pl alt" : Sendscankbd Key_p_lalt
      Case "Rl alt" : Sendscankbd Key_r_lalt
      Case "Pkp /" : Sendscankbd Key_p_kpslash
      Case "Rkp /" : Sendscankbd Key_r_kpslash
      Case "Pn" : Sendscankbd Key_p_n
      Case "Rn" : Sendscankbd Key_r_n
      Case "Pr shft" : Sendscankbd Key_p_rshft
      Case "Rr shft" : Sendscankbd Key_r_rshft
      Case "Pkp *" : Sendscankbd Key_p_kpstar
      Case "Rkp *" : Sendscankbd Key_r_kpstar
      Case "Po" : Sendscankbd Key_p_o
      Case "Ro" : Sendscankbd Key_r_o
      Case "Pr ctrl" : Sendscankbd Key_p_rctrl
      Case "Rr ctrl" : Sendscankbd Key_r_rctrl
      Case "Pkp -" : Sendscankbd Key_p_kpdash
      Case "Rkp -" : Sendscankbd Key_r_kpdash
      Case "Pp" : Sendscankbd Key_p_p
      Case "Rp" : Sendscankbd Key_r_p
      Case "Pr gui" : Sendscankbd Key_p_rgui
      Case "Rr gui" : Sendscankbd Key_r_rgui
      Case "Pkp +" : Sendscankbd Key_p_kpplus
      Case "Rkp +" : Sendscankbd Key_r_kpplus
      Case "Pq" : Sendscankbd Key_p_q
      Case "Rq" : Sendscankbd Key_r_q
      Case "Pr alt" : Sendscankbd Key_p_ralt
      Case "Rr alt" : Sendscankbd Key_r_ralt
      Case "Pkp en" : Sendscankbd Key_p_kpen
      Case "Rkp en" : Sendscankbd Key_r_kpen
      Case "Pr" : Sendscankbd Key_p_r
      Case "Rr" : Sendscankbd Key_r_r
      Case "Papps" : Sendscankbd Key_p_apps
      Case "Rapps" : Sendscankbd Key_r_apps
      Case "Pkp ." : Sendscankbd Key_p_kpdot
      Case "Rkp ." : Sendscankbd Key_r_kpdot
      Case "Ps" : Sendscankbd Key_p_s
      Case "Rs" : Sendscankbd Key_r_s
      Case "Penter" : Sendscankbd Key_p_enter
      Case "Renter" : Sendscankbd Key_r_enter
      Case "Pkp 0" : Sendscankbd Key_p_kp0
      Case "Rkp 0" : Sendscankbd Key_r_kp0
      Case "Pt" : Sendscankbd Key_p_t
      Case "Rt" : Sendscankbd Key_r_t
      Case "Pesc" : Sendscankbd Key_p_esc
      Case "Resc" : Sendscankbd Key_r_esc
      Case "Pkp 1" : Sendscankbd Key_p_kp1
      Case "Rkp 1" : Sendscankbd Key_r_kp1
      Case "Pu" : Sendscankbd Key_p_u
      Case "Ru" : Sendscankbd Key_r_u
      Case "Pf1" : Sendscankbd Key_p_f1
      Case "Rf1" : Sendscankbd Key_r_f1
      Case "Pkp 2" : Sendscankbd Key_p_kp2
      Case "Rkp 2" : Sendscankbd Key_r_kp2
      Case "Pv" : Sendscankbd Key_p_v
      Case "Rv" : Sendscankbd Key_r_v
      Case "Pf2" : Sendscankbd Key_p_f2
      Case "Rf2" : Sendscankbd Key_r_f2
      Case "Pkp 3" : Sendscankbd Key_p_kp3
      Case "Rkp 3" : Sendscankbd Key_r_kp3
      Case "Pw" : Sendscankbd Key_p_w
      Case "Rw" : Sendscankbd Key_r_w
      Case "Pf3" : Sendscankbd Key_p_f3
      Case "Rf3" : Sendscankbd Key_r_f3
      Case "Pkp 4" : Sendscankbd Key_p_kp4
      Case "Rkp 4" : Sendscankbd Key_r_kp4
      Case "Px" : Sendscankbd Key_p_x
      Case "Rx" : Sendscankbd Key_r_x
      Case "Pf4" : Sendscankbd Key_p_f4
      Case "Rf4" : Sendscankbd Key_r_f4
      Case "Pkp 5" : Sendscankbd Key_p_kp5
      Case "Rkp 5" : Sendscankbd Key_r_kp5
      Case "Py" : Sendscankbd Key_p_y
      Case "Ry" : Sendscankbd Key_r_y
      Case "Pf5" : Sendscankbd Key_p_f5
      Case "Rf5" : Sendscankbd Key_r_f5
      Case "Pkp 6" : Sendscankbd Key_p_kp6
      Case "Rkp 6" : Sendscankbd Key_r_kp6
      Case "Pz" : Sendscankbd Key_p_z
      Case "Rz" : Sendscankbd Key_r_z
      Case "Pf6" : Sendscankbd Key_p_f6
      Case "Rf6" : Sendscankbd Key_r_f6
      Case "Pkp 7" : Sendscankbd Key_p_kp7
      Case "Rkp 7" : Sendscankbd Key_r_kp7
      Case "P0" : Sendscankbd Key_p_0
      Case "R0" : Sendscankbd Key_r_0
      Case "Pf7" : Sendscankbd Key_p_f7
      Case "Rf7" : Sendscankbd Key_r_f7
      Case "Pkp 8" : Sendscankbd Key_p_kp8
      Case "Rkp 8" : Sendscankbd Key_r_kp8
      Case "P1" : Sendscankbd Key_p_1
      Case "R1" : Sendscankbd Key_r_1
      Case "Pf8" : Sendscankbd Key_p_f8
      Case "Rf8" : Sendscankbd Key_r_f8
      Case "Pkp 9" : Sendscankbd Key_p_kp9
      Case "Rkp 9" : Sendscankbd Key_r_kp9
      Case "P2" : Sendscankbd Key_p_2
      Case "R2" : Sendscankbd Key_r_2
      Case "Pf9" : Sendscankbd Key_p_f9
      Case "Rf9" : Sendscankbd Key_r_f9
      Case "P]" : Sendscankbd Key_p_closebrace
      Case "R]" : Sendscankbd Key_r_closebrace
      Case "P3" : Sendscankbd Key_p_3
      Case "R3" : Sendscankbd Key_r_3
      Case "Pf10" : Sendscankbd Key_p_f10
      Case "Rf10" : Sendscankbd Key_r_f10
      Case "P;" : Sendscankbd Key_p_semicolon
      Case "R;" : Sendscankbd Key_r_semicolon
      Case "P4" : Sendscankbd Key_p_4
      Case "R4" : Sendscankbd Key_r_4
      Case "Pf11" : Sendscankbd Key_p_f11
      Case "Rf11" : Sendscankbd Key_r_f11
      Case "P'" : Sendscankbd Key_p_qoute
      Case "R'" : Sendscankbd Key_r_qoute
      Case "P5" : Sendscankbd Key_p_5
      Case "R5" : Sendscankbd Key_r_5
      Case "Pf12" : Sendscankbd Key_p_f12
      Case "Rf12" : Sendscankbd Key_r_f12
      Case "P," : Sendscankbd Key_p_comma
      Case "R," : Sendscankbd Key_r_comma
      Case "P6" : Sendscankbd Key_p_6
      Case "R6" : Sendscankbd Key_r_6
      Case "Pprnscrn" : Sendscankbd Key_p_prnscrn
      Case "Rprnscrn" : Sendscankbd Key_r_prnscrn
      Case "P." : Sendscankbd Key_p_dot
      Case "R." : Sendscankbd Key_r_dot
      Case "P7" : Sendscankbd Key_p_7
      Case "R7" : Sendscankbd Key_r_7
      Case "Pscroll" : Sendscankbd Key_p_scroll
      Case "Rscroll" : Sendscankbd Key_r_scroll
      Case "P/" : Sendscankbd Key_p_slash
      Case "R/" : Sendscankbd Key_r_slash
      Case "P8" : Sendscankbd Key_p_8
      Case "R8" : Sendscankbd Key_r_8
      Case "Ppause" : Sendscankbd Key_p_pause

   End Select
End Sub


'======== change key to number =================================================
Function Key2number(byval Values As String) As Byte
   If Values = " " Or Values = "" Then
      Key2number = 0
      Goto End_key2number:
   End If

   Values = Ucase(values)
   Key2number = Asc(values) - 64

End_key2number:

End Function

'======== set default key code to eeprom =======================================
Sub Setdefault_keycode()
   Local Addr As Word
   Local Adr_incr As Byte
   Local Tmp_code As Byte

   Restore Keytable_code
   For Adr_incr = 0 To _dec_all_key
      Read Tmp_code
      Addr = _adr_custom_key_code + Adr_incr
      Writeeeprom Tmp_code , Addr
   Next Adr_incr
End Sub

'======== set default key type to eeprom =======================================
Sub Setdefault_keytype()
   Local Addr As Word
   Local Adr_incr As Byte
   Local Tmp_type As Byte

   Restore Keytable_type
   For Adr_incr = 0 To _dec_all_key
      Read Tmp_type
      Addr = _adr_custom_key_type + Adr_incr
      Writeeeprom Tmp_type , Addr
   Next Adr_incr
End Sub

'======== default button code check with Sendscankbd_press() ===================


$data
Keytable_code:
'
'  F1    1     SK1      SK2      SK3      SK4
'  A     H     O        4        5        [
'  Shft  UP    U        1        2        ]
'  LEFT  DOWN  RIGHT    Combo    Ctrl     ALT
'
Data 56 , 28 , 3 , 15 , 5 , 20
Data 1 , 8 , 15 , 93 , 94 , 71
Data 45 , 78 , 21 , 90 , 91 , 99
Data 79 , 80 , 81 , 254 , 46 , 48

'======== default button type ==================================================
' 1 = normal key     = press on , release off
' 2 = numeric key    = press on , release off (automatic turn numlock key on and off after use)
' 3 = special key    = press once on, press again off, press other key until special key on. 
' 4 = combo key      = press on , release off, but can set macro key + use with mouse ps/2
' 25 = skill 1 key      = looklike normal key but for change in system use by each hero
' 26 = skill 2 key      = looklike normal key but for change in system use by each hero
' 27 = skill 3 key      = looklike normal key but for change in system use by each hero
' 28 = skill 4 key      = looklike normal key but for change in system use by each hero
' 29 = skill 5 key(OPT) = looklike normal key but for change in system use by each hero
' 30 = skill 6 key(OPT) = looklike normal key but for change in system use by each hero
'
Keytable_type:
Data 1 , 1 , 25 , 26 , 27 , 28
Data 1 , 1 , 1 , 2 , 2 , 3
Data 1 , 1 , 4 , 2 , 2 , 3
Data 1 , 1 , 1 , 2 , 2 , 3

'========= key data for send to pc =============================================
Key_p_a:
Data 1 , &H1C
Key_r_a:
Data 2 , &HF0 , &H1C

Key_p_9:
Data 1 , &H46
Key_r_9:
Data 2 , &HF0 , &H46

Key_p_openbrace:
Data 1 , &H54
Key_r_openbrace:
Data 2 , &HF0 , &H54

Key_p_b:
Data 1 , &H32
Key_r_b:
Data 2 , &HF0 , &H32

Key_p_backqoute:
Data 1 , &H0E
Key_r_backqoute:
Data 2 , &HF0 , &H0E

Key_p_insert:
Data 2 , &HE0 , &H70
Key_r_insert:
Data 3 , &HE0 , &HF0 , &H70

Key_p_c:
Data 1 , &H21
Key_r_c:
Data 2 , &HF0 , &H21

Key_p_dash:
Data 1 , &H4E
Key_r_dash:
Data 2 , &HF0 , &H4E

Key_p_home:
Data 2 , &HE0 , &H6C
Key_r_home:
Data 3 , &HE0 , &HF0 , &H6C

Key_p_d:
Data 1 , &H23
Key_r_d:
Data 2 , &HF0 , &H23

Key_p_equal:
Data 1 , &H55
Key_r_equal:
Data 2 , &HF0 , &H55

Key_p_pgup:
Data 2 , &HE0 , &H7D
Key_r_pgup:
Data 3 , &HE0 , &HF0 , &H7D

Key_p_e:
Data 1 , &H24
Key_r_e:
Data 2 , &HF0 , &H24

Key_p_backslash:
Data 1 , &H5D
Key_r_backslash:
Data 2 , &HF0 , &H5D

Key_p_delete:
Data 2 , &HE0 , &H71
Key_r_delete:
Data 3 , &HE0 , &HF0 , &H71

Key_p_f:
Data 1 , &H2B
Key_r_f:
Data 2 , &HF0 , &H2B

Key_p_bksp:
Data 1 , &H66
Key_r_bksp:
Data 2 , &HF0 , &H66

Key_p_end:
Data 2 , &HE0 , &H69
Key_r_end:
Data 3 , &HE0 , &HF0 , &H69

Key_p_g:
Data 1 , &H34
Key_r_g:
Data 2 , &HF0 , &H34

Key_p_space:
Data 1 , &H29
Key_r_space:
Data 2 , &HF0 , &H29

Key_p_pgdn:
Data 2 , &HE0 , &H7A
Key_r_pgdn:
Data 3 , &HE0 , &HF0 , &H7A

Key_p_h:
Data 1 , &H33
Key_r_h:
Data 2 , &HF0 , &H33

Key_p_tab:
Data 1 , &H0D
Key_r_tab:
Data 2 , &HF0 , &H0D

Key_p_uarrow:
Data 2 , &HE0 , &H75
Key_r_uarrow:
Data 3 , &HE0 , &HF0 , &H75

Key_p_i:
Data 1 , &H43
Key_r_i:
Data 2 , &HF0 , &H43

Key_p_caps:
Data 1 , &H58
Key_r_caps:
Data 2 , &HF0 , &H58

Key_p_larrow:
Data 2 , &HE0 , &H6B
Key_r_larrow:
Data 3 , &HE0 , &HF0 , &H6B

Key_p_j:
Data 1 , &H3B
Key_r_j:
Data 2 , &HF0 , &H3B

Key_p_lshft:
Data 1 , &H12
Key_r_lshft:
Data 2 , &HF0 , &H12

Key_p_darrow:
Data 2 , &HE0 , &H72
Key_r_darrow:
Data 3 , &HE0 , &HF0 , &H72

Key_p_k:
Data 1 , &H42
Key_r_k:
Data 2 , &HF0 , &H42

Key_p_lctrl:
Data 1 , &H14
Key_r_lctrl:
Data 2 , &HF0 , &H14

Key_p_rarrow:
Data 2 , &HE0 , &H74
Key_r_rarrow:
Data 3 , &HE0 , &HF0 , &H74

Key_p_l:
Data 1 , &H4B
Key_r_l:
Data 2 , &HF0 , &H4B

Key_p_lgui:
Data 2 , &HE0 , &H1F
Key_r_lgui:
Data 3 , &HE0 , &HF0 , &H1F

Key_p_num:
Data 1 , &H77
Key_r_num:
Data 2 , &HF0 , &H77

Key_p_m:
Data 1 , &H3A
Key_r_m:
Data 2 , &HF0 , &H3A

Key_p_lalt:
Data 1 , &H11
Key_r_lalt:
Data 2 , &HF0 , &H11

Key_p_kpslash:
Data 2 , &HE0 , &H4A
Key_r_kpslash:
Data 3 , &HE0 , &HF0 , &H4A

Key_p_n:
Data 1 , &H31
Key_r_n:
Data 2 , &HF0 , &H31

Key_p_rshft:
Data 1 , &H59
Key_r_rshft:
Data 2 , &HF0 , &H59

Key_p_kpstar:
Data 1 , &H7C
Key_r_kpstar:
Data 2 , &HF0 , &H7C

Key_p_o:
Data 1 , &H44
Key_r_o:
Data 2 , &HF0 , &H44

Key_p_rctrl:
Data 2 , &HE0 , &H14
Key_r_rctrl:
Data 3 , &HE0 , &HF0 , &H14

Key_p_kpdash:
Data 1 , &H7B
Key_r_kpdash:
Data 2 , &HF0 , &H7B

Key_p_p:
Data 1 , &H4D
Key_r_p:
Data 2 , &HF0 , &H4D

Key_p_rgui:
Data 2 , &HE0 , &H27
Key_r_rgui:
Data 3 , &HE0 , &HF0 , &H27

Key_p_kpplus:
Data 1 , &H79
Key_r_kpplus:
Data 2 , &HF0 , &H79

Key_p_q:
Data 1 , &H15
Key_r_q:
Data 2 , &HF0 , &H15

Key_p_ralt:
Data 2 , &HE0 , &H11
Key_r_ralt:
Data 3 , &HE0 , &HF0 , &H11

Key_p_kpen:
Data 2 , &HE0 , &H5A
Key_r_kpen:
Data 3 , &HE0 , &HF0 , &H5A

Key_p_r:
Data 1 , &H2D
Key_r_r:
Data 2 , &HF0 , &H2D

Key_p_apps:
Data 2 , &HE0 , &H2F
Key_r_apps:
Data 3 , &HE0 , &HF0 , &H2F

Key_p_kpdot:
Data 1 , &H71
Key_r_kpdot:
Data 2 , &HF0 , &H71

Key_p_s:
Data 1 , &H1B
Key_r_s:
Data 2 , &HF0 , &H1B

Key_p_enter:
Data 1 , &H5A
Key_r_enter:
Data 2 , &HF0 , &H5A

Key_p_kp0:
Data 1 , &H70
Key_r_kp0:
Data 2 , &HF0 , &H70

Key_p_t:
Data 1 , &H2C
Key_r_t:
Data 2 , &HF0 , &H2C

Key_p_esc:
Data 1 , &H76
Key_r_esc:
Data 2 , &HF0 , &H76

Key_p_kp1:
Data 1 , &H69
Key_r_kp1:
Data 2 , &HF0 , &H69

Key_p_u:
Data 1 , &H3C
Key_r_u:
Data 2 , &HF0 , &H3C

Key_p_f1:
Data 1 , &H5
Key_r_f1:
Data 2 , &HF0 , &H05

Key_p_kp2:
Data 1 , &H72
Key_r_kp2:
Data 2 , &HF0 , &H72

Key_p_v:
Data 1 , &H2A
Key_r_v:
Data 2 , &HF0 , &H2A

Key_p_f2:
Data 1 , &H6
Key_r_f2:
Data 2 , &HF0 , &H06

Key_p_kp3:
Data 1 , &H7A
Key_r_kp3:
Data 2 , &HF0 , &H7A

Key_p_w:
Data 1 , &H1D
Key_r_w:
Data 2 , &HF0 , &H1D

Key_p_f3:
Data 1 , &H4
Key_r_f3:
Data 2 , &HF0 , &H04

Key_p_kp4:
Data 1 , &H6B
Key_r_kp4:
Data 2 , &HF0 , &H6B

Key_p_x:
Data 1 , &H22
Key_r_x:
Data 2 , &HF0 , &H22

Key_p_f4:
Data 1 , &H0C
Key_r_f4:
Data 2 , &HF0 , &H0C

Key_p_kp5:
Data 1 , &H73
Key_r_kp5:
Data 2 , &HF0 , &H73

Key_p_y:
Data 1 , &H35
Key_r_y:
Data 2 , &HF0 , &H35

Key_p_f5:
Data 1 , &H3
Key_r_f5:
Data 2 , &HF0 , &H03

Key_p_kp6:
Data 1 , &H74
Key_r_kp6:
Data 2 , &HF0 , &H74

Key_p_z:
Data 1 , &H1A
Key_r_z:
Data 2 , &HF0 , &H1A

Key_p_f6:
Data 1 , &H0B
Key_r_f6:
Data 2 , &HF0 , &H0B

Key_p_kp7:
Data 1 , &H6C
Key_r_kp7:
Data 2 , &HF0 , &H6C

Key_p_0:
Data 1 , &H45
Key_r_0:
Data 2 , &HF0 , &H45

Key_p_f7:
Data 1 , &H83
Key_r_f7:
Data 2 , &HF0 , &H83

Key_p_kp8:
Data 1 , &H75
Key_r_kp8:
Data 2 , &HF0 , &H75

Key_p_1:
Data 1 , &H16
Key_r_1:
Data 2 , &HF0 , &H16

Key_p_f8:
Data 1 , &H0A
Key_r_f8:
Data 2 , &HF0 , &H0A

Key_p_kp9:
Data 1 , &H7D
Key_r_kp9:
Data 2 , &HF0 , &H7D

Key_p_2:
Data 1 , &H1E
Key_r_2:
Data 2 , &HF0 , &H1E

Key_p_f9:
Data 1 , &H1
Key_r_f9:
Data 2 , &HF0 , &H01

Key_p_closebrace:
Data 1 , &H5B
Key_r_closebrace:
Data 2 , &HF0 , &H5B

Key_p_3:
Data 1 , &H26
Key_r_3:
Data 2 , &HF0 , &H26

Key_p_f10:
Data 1 , &H9
Key_r_f10:
Data 2 , &HF0 , &H09

Key_p_semicolon:
Data 1 , &H4C
Key_r_semicolon:
Data 2 , &HF0 , &H4C

Key_p_4:
Data 1 , &H25
Key_r_4:
Data 2 , &HF0 , &H25

Key_p_f11:
Data 1 , &H78
Key_r_f11:
Data 2 , &HF0 , &H78

Key_p_qoute:
Data 1 , &H52
Key_r_qoute:
Data 2 , &HF0 , &H52

Key_p_5:
Data 1 , &H2E
Key_r_5:
Data 2 , &HF0 , &H2E

Key_p_f12:
Data 1 , &H7
Key_r_f12:
Data 2 , &HF0 , &H07

Key_p_comma:
Data 1 , &H41
Key_r_comma:
Data 2 , &HF0 , &H41

Key_p_6:
Data 1 , &H36
Key_r_6:
Data 2 , &HF0 , &H36

Key_p_prnscrn:
Data 4 , &HE0 , &H12 , &HE0 , &H7C
Key_r_prnscrn:
Data 6 , &HE0 , &HF0 , &H7C , &HE0 , &HF0 , &H12

Key_p_dot:
Data 1 , &H49
Key_r_dot:
Data 2 , &HF0 , &H49

Key_p_7:
Data 1 , &H3D
Key_r_7:
Data 2 , &HF0 , &H3D

Key_p_scroll:
Data 1 , &H7E
Key_r_scroll:
Data 2 , &HF0 , &H7E

Key_p_slash:
Data 1 , &H4A
Key_r_slash:
Data 2 , &HF0 , &H4A

Key_p_8:
Data 1 , &H3E
Key_r_8:
Data 2 , &HF0 , &H3E

Key_p_pause:
Data 8 , &HE1 , &H14 , &H77 , &HE1 , &HF0 , &H14 , &HF0 , &H77


Key_p_power:
Data 2 , &HE0 , &H37
Key_r_power:
Data 3 , &HE0 , &HF0 , &H37

Key_p_sleep:
Data 2 , &HE0 , &H3F
Key_r_sleep:
Data 3 , &HE0 , &HF0 , &H3F

Key_p_wake:
Data 2 , &HE0 , &H5E
Key_r_wake:
Data 3 , &HE0 , &HF0 , &H5E



Key_p_next_track:
Data 2 , &HE0 , &H4D
Key_r_next_track:
Data 3 , &HE0 , &HF0 , &H4D

Key_p_previous_track:
Data 2 , &HE0 , &H15
Key_r_previous_track:
Data 3 , &HE0 , &HF0 , &H15

Key_p_stop:
Data 2 , &HE0 , &H3B
Key_r_stop:
Data 3 , &HE0 , &HF0 , &H3B

Key_p_play_pause:
Data 2 , &HE0 , &H34
Key_r_play_pause:
Data 3 , &HE0 , &HF0 , &H34

Key_p_mute:
Data 2 , &HE0 , &H23
Key_r_mute:
Data 3 , &HE0 , &HF0 , &H23

Key_p_volume_up:
Data 2 , &HE0 , &H32
Key_r_volume_up:
Data 3 , &HE0 , &HF0 , &H32

Key_p_volume_down:
Data 2 , &HE0 , &H21
Key_r_volume_down:
Data 3 , &HE0 , &HF0 , &H21

Key_p_media_select:
Data 2 , &HE0 , &H50
Key_r_media_select:
Data 3 , &HE0 , &HF0 , &H50

Key_p_e_mail:
Data 2 , &HE0 , &H48
Key_r_e_mail:
Data 3 , &HE0 , &HF0 , &H48

Key_p_calculator:
Data 2 , &HE0 , &H2B
Key_r_calculator:
Data 3 , &HE0 , &HF0 , &H2B

Key_p_my_computer:
Data 2 , &HE0 , &H40
Key_r_my_computer:
Data 3 , &HE0 , &HF0 , &H40

Key_p_www_search:
Data 2 , &HE0 , &H10
Key_r_www_search:
Data 3 , &HE0 , &HF0 , &H10

Key_p_www_home:
Data 2 , &HE0 , &H3A
Key_r_www_home:
Data 3 , &HE0 , &HF0 , &H3A

Key_p_www_back:
Data 2 , &HE0 , &H38
Key_r_www_back:
Data 3 , &HE0 , &HF0 , &H38

Key_p_www_forward:
Data 2 , &HE0 , &H30
Key_r_www_forward:
Data 3 , &HE0 , &HF0 , &H30

Key_p_www_stop:
Data 2 , &HE0 , &H28
Key_r_www_stop:
Data 3 , &HE0 , &HF0 , &H28

Key_p_www_refresh:
Data 2 , &HE0 , &H20
Key_r_www_refresh:
Data 3 , &HE0 , &HF0 , &H20

Key_p_www_favorites:
Data 2 , &HE0 , &H18
Key_r_www_favorites:
Data 3 , &HE0 , &HF0 , &H18


'===============================================================================
' hero key data and name
' format
'
' No., Hero Name (30), all skill, sk1,sk2,;;; skn
'

Group_1:
Data 1 , "The Moon Rider" , 4 , "c" , " " , " " , "e"
Data 2 , "The Dwarven Sniper" , 4 , "c" , "o" , "e" , "t"
Data 3 , "The Troll Warlord" , 4 , "g" , "d" , " " , "r"
Data 4 , "Shadow Shaman" , 4 , "r" , "d" , "e" , "w"
Data 5 , "The Bristleback" , 4 , "g" , "r" , " " , " "
Data 6 , "The Pandaren Battlemaster" , 4 , "c" , "d" , " " , "r"
Data 7 , "The Centaur Warchief" , 4 , "f" , "d" , " " , " "
Data 8 , "The Bounty Hunter" , 4 , "t" , " " , "w" , "r"
Data 9 , "The Dragon Night" , 4 , "f" , "t" , " " , "r"
Data 10 , "The Anti-Mage" , 4 , " " , "b" , " " , "v"
Data 11 , "The Drow Ranger" , 4 , "r" , "e" , " " , " "
Data 12 , "The Omniknight" , 4 , "r" , "e" , " " , "g"
Data 13 , "The Vengeful Spirit" , 4 , "c" , "t" , " " , "w"
Data 14 , "The Lord of Olympia" , 4 , "c" , "g" , " " , "w"
Data 15 , "The Enchantress" , 4 , "t" , "c" , "r" , " "
Data 16 , "The Morphling" , 4 , "w" , "e" , " " , "d"

Group_2:
Data 17 , "The Crystal Maiden" , 4 , "v" , "e" , " " , "z"
Data 18 , "The RogueKnight" , 4 , "t" , " " , " " , "r"
Data 19 , "The Naga Siren" , 4 , "r" , "e" , " " , "g"
Data 20 , "The EarthShaker" , 4 , "f" , "e" , " " , "c"
Data 21 , "The Stealth Assassin" , 4 , "c" , "b" , " " , " "
Data 22 , "The Lone Druid" , 4 , "b" , "r" , " " , "f"
Data 23 , "The Slayer" , 4 , "d" , "t" , "e" , "g"
Data 24 , "The Juggernaut" , 4 , "f" , "g" , " " , "n"
Data 25 , "The Silencer" , 4 , "c" , "g" , " " , "e"
Data 26 , "The Treant Protector" , 4 , "t" , "e" , "v" , "r"
Data 27 , "The Enigma" , 4 , "f" , "c" , "d" , "b"
Data 28 , "The Keeper of the Light" , 4 , "t" , "e" , "c" , "f"
Data 29 , "The Ursa Warrior" , 4 , "e" , "v" , " " , "r"
Data 30 , "The Ogre Magi" , 4 , "f" , "g" , "b" , " "
Data 31 , "The Tinker" , 4 , "e" , "t" , "c" , "r"
Data 32 , "The Prophet" , 4 , "t" , "r" , "f" , "w"

Group_3:
Data 33 , "The Phantom Lancer" , 4 , "t" , "w" , " " , " "
Data 34 , "The Stone Giant" , 4 , "v" , "t" , " " , " "
Data 35 , "The Goblin Techies" , 4 , "e" , "t" , "c" , "r"
Data 36 , "The Holy Knight" , 4 , "e" , "t" , "r" , "d"
Data 37 , "The Soul Keeper" , 4 , "t" , "e" , "c" , "r"
Data 38 , "The Tormented Soul" , 4 , "t" , "c" , "g" , "v"
Data 39 , "The Lich" , 4 , "v" , "f" , "d" , "c"
Data 40 , "The Death Prophet" , 4 , "r" , "e" , " " , "x"
Data 41 , "The Demon Witch" , 4 , "e" , "d" , "r" , "f"
Data 42 , "The Venomancer" , 4 , "d" , " " , "w" , "v"
Data 43 , "The Magnataur" , 4 , "w" , "e" , " " , "v"
Data 44 , "The Necro'Lic" , 4 , "g" , "t" , " " , "v"
Data 45 , "The Chaos Knight" , 4 , "c" , "b" , " " , "t"
Data 46 , "The Lycanthrope" , 4 , "v" , "w" , " " , "f"
Data 47 , "The BloodMother" , 4 , "w" , "d" , " " , "t"
Data 48 , "The Phantom Assassin" , 4 , "d" , "b" , " " , " "

Group_4:
Data 49 , "The Oblivion" , 4 , "b" , "c" , "w" , "d"
Data 50 , "The Tidehunter" , 4 , "g" , " " , " " , "v"
Data 51 , "The Bane Elemental" , 4 , "e" , "b" , "t" , "f"
Data 52 , "The Necrolyte" , 4 , "d" , " " , " " , "r"
Data 53 , "The Butcher" , 4 , "t" , "r" , " " , "d"
Data 54 , "The Spirit Breaker" , 4 , "c" , " " , " " , "e"
Data 55 , "The Nerubian Weaver" , 4 , "w" , "c" , " " , "t"
Data 56 , "The Shadow Fiend" , 4 , "z" , " " , " " , "r"
Data 57 , "The Sand King" , 4 , "e" , "r" , " " , "c"
Data 58 , "The Axe" , 4 , "e" , "r" , " " , "c"
Data 59 , "The BloodSeeker" , 4 , "d" , " " , "t" , "r"
Data 60 , "The Lord of Avernus" , 4 , "d" , "t" , " " , " "
Data 61 , "The Gorgon" , 4 , "t" , "c" , "e" , "g"
Data 62 , "The Night Stalker" , 4 , "v" , " " , " " , "r"
Data 63 , "The Skeleton King" , 4 , "t" , " " , " " , " "
Data 64 , "The Doom Bringer" , 4 , "e" , "t" , "v" , "d"

Group_5:
Data 65 , "The Nerubian Assassin" , 4 , "e" , "r" , " " , "v"
Data 66 , "The Slithereen Guard" , 4 , "t" , "r" , " " , "g"
Data 67 , "The Queen of Pain" , 4 , "d" , "b" , "f" , "w"
Data 68 , "The Bone Fletcher" , 4 , "t" , "r" , "w" , "e"
Data 69 , "The Faceless Void" , 4 , "w" , " " , " " , "c"
Data 70 , "The NetherDrake" , 4 , "r" , "c" , " " , "v"
Data 71 , "The Lightning Revenant" , 4 , "r" , "c" , " " , " "
Data 72 , "The LifeStealer" , 4 , " " , " " , " " , "r"
Data 73 , "The Twin Head Dragon" , 4 , "d" , "t" , " " , "r"
Data 74 , "The BeastMaster" , 4 , "w" , "d" , " " , "r"
Data 75 , "The Spectre" , 4 , "d" , " " , " " , "t"
Data 76 , "The Witch Doctor" , 4 , "c" , "r" , "e" , "d"
Data 77 , "The Obsidian Destroyer" , 4 , "r" , "t" , " " , "c"

Group_6:
Data 255 , "Not Found Hero" , 4 , " " , " " , " " , " "