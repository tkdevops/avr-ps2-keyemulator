# AVR-PS2-KeyEmulator

<img src="images/Dota_Artwork.jpg" width="448" height="336"><br />

ซอร์สโค้ด library "ปุ่มอัตโนมัติ" เพื่อการเล่นเกม DOTA (full source code product "Hardware Macro Keyboard" using AVR microcontroller)<br />
เป็น PS/2 library ที่สามารถเข้ากันได้กับ keypad ขนาด 6x4 ที่มีขายทั่วไป โดยในที่นี่ได้นำมาประยุกต์ใช้เป็นมาโครคีย์กับเกม DotA <br />
<br />
เกม DotA นั้นมีระบบป้องกันการใช้ software macro key แต่การเล่นเกมนี้ผ่าน keyboard ของ notebook เป็นความยุ่งยากมากๆ และยิ่งถ้าเปลี่ยน Hero <br /> ก็จะต้องเปลี่ยนปุ่มกด Skill ไปด้วยก็ยิ่งยุ่งยากไปอีก <br />   <br />
ผู้พัฒนาได้เห็นความยุ่งยาก จึงได้เริ่มพัฒนา macro keyboard ด้วย Microcontoller ATMEGA32 เพื่อช่วยให้การกด skill ต่าง ๆ เป็นไปได้โดยง่าย <br />
โดยใช้คู่กับแป้นกดของบริษัท ETT และจำลองเป็น keyboard ps/2 (ผ่านตัวแปลง PS/2-to-USB) <br />
<img src="images/ETT-keyboard.jpg" width="448" height="336"><br />

# รายละเอียด
- Project   : PS/2 Keyboard for DotA Gamer
- Date      : 4/01/2007 (new) : 27/07/2006(Old)
- MCU       : ATMEGA32 8Mhz
- Compiler  : BasComAVR

# จุดเด่น
- สามารถทำ combo skill ได้ 
- กด skill ในรูปแบบสั่งเปิด/ปิด ได้ (ไม่ต้องกดค้าง)
- บันทึกปุ่ม ลง eeprom ในตัว
- กดได้หลายปุ่มพร้อมกัน
- สามารถดึง library ไปใช้งาน แทน Keypad library เดิมได้เลยทันที

# สรุปการทดลอง
สามารถทำงานได้เป็นอย่างดี  และไม่มีผล effect ใดๆ  ต่อโปรแกรมที่ป้องกันก่อนเข้าเกม  ทำให้การเล่นเกมส์ทั้ง DotA และอื่นๆ สามารถทำได้สะดวกมาก :) <br/>
ซึ่งนอกเหนือจากการเล่นเกมส์ต่างๆ แล้ว ยังสามารถประยุกต์ใช้เป็น macro keyboard อเนกประสงค์ สำหรับงานซ้ำๆ ไม่จำกัดรูปแบบได้สบาย<br />
อย่างไรก็ดี ด้วยรูปแบบ interface เป็นแบบ PS/2 ซึ่งเก่าเกินไปแล้ว  ในรุ่นต่อมาจึงพัฒนาเป็น USB

`TKVR`
